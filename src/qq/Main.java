package qq;
import ftp.Client;
import ftp.Server;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        boolean s = false, c = false;
        String stport = "0", stip = "0";
        for(int i = 0; i < args.length; ++i){
            if(args[i].equals("-s")){
                s = true;
            }
            if(args[i].equals("-c")){
                c = true;
            }
            if(args[i].equals("-ip")){
                stip = args[i + 1];
            }
            if(args[i].equals("-port")){
                stport = args[i + 1];
            }
        }
        if(s){
            try{
                Server server = new Server(Integer.parseInt(stport));
                server.start();
            }
            catch (IOException e) {
                System.out.println("error" + e);
            }

        }
        if(c){
            try{
                InetAddress ip = InetAddress.getByName(stip);
                Client client = new Client(ip, Integer.parseInt(stport));
                client.send();
                client.get();
            }
            catch(UnknownHostException e){
                System.out.println("Unknown host error");
            }
            catch (IOException e) {
                System.out.println("error" + e);
            }
        }
    }
}