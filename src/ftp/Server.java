package ftp;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;

public class Server {
    private int port;
    private ServerSocket server;
    public static LinkedList<UserConnect> list = new LinkedList<>();
    public Server(int port_) throws IOException {
        port = port_;
        server = new ServerSocket(port);
    }
    public void start(){
        while(!server.isClosed()){
            try {
                Socket socket = server.accept();
                list.add(new UserConnect(socket));
            } catch(IOException e) {
                System.err.println("error :" + e);
            }
        }
    }
    public void stop() throws IOException{
        server.close();
    }
}