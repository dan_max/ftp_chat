package ftp;


import java.io.*;
import java.net.Socket;

public class UserConnect extends Thread {
    private Socket socket;
    private BufferedReader in;
    private BufferedWriter out;
    public UserConnect(Socket socket_) throws IOException {
        socket = socket_;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        start();
    }
    @Override
    public void run(){
        String msg;
        try{
            while (true) {
                msg = in.readLine();
                if(msg.equals("@quit")){
                    Server.list.remove(this);
                    break;
                }
                for(UserConnect vr : Server.list){
                    if(vr == this){
                        //vr.send(msg + "\n");
                    }
                    else {
                        vr.send("\t" + msg + "\n");
                    }
                }
            }
        }
        catch(IOException e){
            System.err.println("error :" + e);
        }
    }
    private void send(String msg){
        try{
            out.write(msg);
            out.flush();
        }
        catch (IOException e) {
            System.err.println("error :" + e);
        }
    }
}