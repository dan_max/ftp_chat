package ftp;


import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

public class Client {
    private InetAddress ip;
    private int port;
    private BufferedReader reader;
    private BufferedReader in;
    private BufferedWriter out;
    private String name;
    Socket client;
    public Client(InetAddress ip_, int port_) throws IOException {
        ip = ip_;
        port = port_;
        client = new Socket(ip , port);
        reader = new BufferedReader(new InputStreamReader(System.in));
        in = new BufferedReader(new InputStreamReader(client.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
    }
    public void sendmsg() throws IOException {
        if(name == null){
            System.out.println("input name first");
            name = reader.readLine();
        }
        boolean not_command = true;
        String msg = reader.readLine();
        if(msg.equals("@quit")){
            System.out.print("Buy");
            out.write(msg);
            out.flush();
            not_command = false;
            stop();
        }
        if(msg.equals("@name")){
            String temp = msg.replaceFirst("@name", "");
            temp = temp.trim();
            name = temp;
            not_command = false;
        }
        if(not_command) {
            out.write(name + ":" + msg + "\n");
            out.flush();
        }
    }

    public void getmsg() throws IOException {
        String msg = in.readLine();
        System.out.println(msg);
    }
    public void send(){
        Sender s = new Sender();
        s.start();
    }
    public void get(){
        Getter s = new Getter();
        s.start();
    }

    private class Sender extends Thread{
        private Sender(){
            super("Sender");
        }
        @Override
        public void run(){
            while(!client.isClosed()){
                try {
                    sendmsg();
                } catch(IOException e) {
                    System.err.println("Send error" + e);
                }
            }
        }
    }

    private class Getter extends Thread{
        private Getter(){
            super("Getter");
        }
        @Override
        public void run(){
            while(!client.isClosed()){
                try{
                    getmsg();
                }
                catch (IOException e) {
                    System.err.println("Get error" + e);
                }
            }
        }
    }

    private void stop() throws IOException {
        client.close();
    }
}